-- phpMyAdmin SQL Dump
-- version 4.8.5
-- https://www.phpmyadmin.net/
--
-- Хост: 127.0.0.1:3306
-- Время создания: Ноя 20 2020 г., 16:46
-- Версия сервера: 5.6.43
-- Версия PHP: 7.2.10

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- База данных: `phalcon`
--

-- --------------------------------------------------------

--
-- Структура таблицы `days_off`
--

CREATE TABLE `days_off` (
  `id` int(11) NOT NULL,
  `month` int(11) NOT NULL,
  `day` int(11) NOT NULL,
  `repeat` enum('Y','N') NOT NULL,
  `date` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `days_off`
--

INSERT INTO `days_off` (`id`, `month`, `day`, `repeat`, `date`) VALUES
(14, 11, 16, 'N', '2020'),
(16, 11, 10, 'N', '2020');

-- --------------------------------------------------------

--
-- Структура таблицы `hours`
--

CREATE TABLE `hours` (
  `id` int(10) UNSIGNED NOT NULL,
  `usersId` int(10) UNSIGNED NOT NULL,
  `total` varchar(12) DEFAULT NULL,
  `less` varchar(12) DEFAULT NULL,
  `late` tinyint(1) NOT NULL DEFAULT '0',
  `createdAt` date NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `hours`
--

INSERT INTO `hours` (`id`, `usersId`, `total`, `less`, `late`, `createdAt`) VALUES
(24, 1, '10:00', NULL, 0, '2020-11-19'),
(26, 1, '09:30', NULL, 0, '2020-11-20'),
(27, 2, '06:46', '02:14:00', 0, '2020-11-20');

-- --------------------------------------------------------

--
-- Структура таблицы `months`
--

CREATE TABLE `months` (
  `id` int(11) NOT NULL,
  `name` varchar(255) NOT NULL,
  `value` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `months`
--

INSERT INTO `months` (`id`, `name`, `value`) VALUES
(1, 'January', '1'),
(2, 'February', '2'),
(3, 'March', '3'),
(4, 'April', '4'),
(5, 'May', '5'),
(6, 'June', '6'),
(7, 'Jule', '7'),
(8, 'August', '8'),
(9, 'September', '9\r\n'),
(10, 'October', '10'),
(11, 'November', '11'),
(12, 'December', '12');

-- --------------------------------------------------------

--
-- Структура таблицы `permissions`
--

CREATE TABLE `permissions` (
  `id` int(11) NOT NULL,
  `roleId` int(11) NOT NULL,
  `resource` varchar(255) NOT NULL,
  `action` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `permissions`
--

INSERT INTO `permissions` (`id`, `roleId`, `resource`, `action`) VALUES
(1, 1, 'index', 'index');

-- --------------------------------------------------------

--
-- Структура таблицы `settings`
--

CREATE TABLE `settings` (
  `id` int(10) UNSIGNED NOT NULL,
  `key` varchar(255) NOT NULL,
  `value` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `settings`
--

INSERT INTO `settings` (`id`, `key`, `value`) VALUES
(8, 'beginning', '09:00'),
(9, 'max_late', '3');

-- --------------------------------------------------------

--
-- Структура таблицы `start_end`
--

CREATE TABLE `start_end` (
  `id` int(10) UNSIGNED NOT NULL,
  `hourId` int(10) UNSIGNED NOT NULL,
  `start` varchar(10) DEFAULT NULL,
  `stop` varchar(10) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `start_end`
--

INSERT INTO `start_end` (`id`, `hourId`, `start`, `stop`) VALUES
(68, 24, '08:29:31', '18:29:32'),
(71, 24, NULL, NULL),
(75, 26, '08:00:00', '15:30:00'),
(76, 26, '08:29:31', '10:29:32'),
(77, 26, '09:24:00', '09:24:04'),
(78, 26, '10:47:40', '10:47:43'),
(79, 26, NULL, NULL),
(80, 27, '09:00:00', '11:00:00'),
(81, 27, '11:39:10', '15:39:10'),
(82, 27, '15:58:56', '16:45:44'),
(83, 27, NULL, NULL);

-- --------------------------------------------------------

--
-- Структура таблицы `users`
--

CREATE TABLE `users` (
  `id` int(10) UNSIGNED NOT NULL,
  `lastname` varchar(255) NOT NULL,
  `username` varchar(255) NOT NULL,
  `password` varchar(255) NOT NULL,
  `email` varchar(255) NOT NULL,
  `active` varchar(255) NOT NULL DEFAULT 'Y',
  `created_at` int(11) NOT NULL,
  `userRole` int(10) UNSIGNED NOT NULL DEFAULT '2'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `users`
--

INSERT INTO `users` (`id`, `lastname`, `username`, `password`, `email`, `active`, `created_at`, `userRole`) VALUES
(1, 'Иван Попов', 'admin', 'd033e22ae348aeb5660fc2140aec35850c4da997', 'admin@mail.com', 'Y', 2147483647, 1),
(2, 'John Legend', 'john', 'b1b3773a05c0ed0176787a4f1574ff0075f7521e', 'admin2@mail.com', 'Y', 2147483647, 2),
(3, 'Ivan Kolesnikov', 'ivan', 'da39a3ee5e6b4b0d3255bfef95601890afd80709', 'ivan@mail.comkk', 'Y', 2147483647, 3),
(7, 'Johnny Depp', 'admikn', 'da39a3ee5e6b4b0d3255bfef95601890afd80709', 'admlkin@mail.com', 'Y', 2147483647, 2),
(8, 'johnny depp', 'adkmin2', 'da39a3ee5e6b4b0d3255bfef95601890afd80709', 'adjmin2@mail.com', 'Y', 2147483647, 2),
(9, 'Ivan Kolesnikov', 'ivajn', 'd033e22ae348aeb5660fc2140aec35850c4da997', 'ivjan@mail.com', 'Y', 0, 2),
(10, 'User', 'test', '7110eda4d09e062aa5e4a390b0a572ac0d2c0220', 'test@jj.cc', 'Y', 2147483647, 2),
(11, 'User', 'admin3', 'da39a3ee5e6b4b0d3255bfef95601890afd80709', 'whoamirk@gmail.com', 'Y', 2147483647, 2),
(12, 'test', 'tests', '40bd001563085fc35165329ea1ff5c5ecbdbbeef', 'test@jj.ccd', 'Y', 2147483647, 2),
(15, 'ali', 'ali', '40bd001563085fc35165329ea1ff5c5ecbdbbeef', 'ali@jll.kk', 'Y', 2147483647, 3),
(16, 'era', 'era', '40bd001563085fc35165329ea1ff5c5ecbdbbeef', 'era@ll.cl', 'Y', 2147483647, 2),
(17, 'mom', 'mom', '40bd001563085fc35165329ea1ff5c5ecbdbbeef', 'mom@ll.ll', 'Y', 2147483647, 1);

-- --------------------------------------------------------

--
-- Структура таблицы `user_roles`
--

CREATE TABLE `user_roles` (
  `id` int(11) NOT NULL,
  `name` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `user_roles`
--

INSERT INTO `user_roles` (`id`, `name`) VALUES
(1, 'admins'),
(2, 'users'),
(3, 'guests');

--
-- Индексы сохранённых таблиц
--

--
-- Индексы таблицы `days_off`
--
ALTER TABLE `days_off`
  ADD PRIMARY KEY (`id`);

--
-- Индексы таблицы `hours`
--
ALTER TABLE `hours`
  ADD PRIMARY KEY (`id`),
  ADD KEY `usersId` (`usersId`);

--
-- Индексы таблицы `months`
--
ALTER TABLE `months`
  ADD PRIMARY KEY (`id`);

--
-- Индексы таблицы `permissions`
--
ALTER TABLE `permissions`
  ADD PRIMARY KEY (`id`);

--
-- Индексы таблицы `settings`
--
ALTER TABLE `settings`
  ADD PRIMARY KEY (`id`);

--
-- Индексы таблицы `start_end`
--
ALTER TABLE `start_end`
  ADD PRIMARY KEY (`id`),
  ADD KEY `hourId` (`hourId`);

--
-- Индексы таблицы `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`);

--
-- Индексы таблицы `user_roles`
--
ALTER TABLE `user_roles`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT для сохранённых таблиц
--

--
-- AUTO_INCREMENT для таблицы `days_off`
--
ALTER TABLE `days_off`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=17;

--
-- AUTO_INCREMENT для таблицы `hours`
--
ALTER TABLE `hours`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=28;

--
-- AUTO_INCREMENT для таблицы `months`
--
ALTER TABLE `months`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=13;

--
-- AUTO_INCREMENT для таблицы `permissions`
--
ALTER TABLE `permissions`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT для таблицы `settings`
--
ALTER TABLE `settings`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=10;

--
-- AUTO_INCREMENT для таблицы `start_end`
--
ALTER TABLE `start_end`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=84;

--
-- AUTO_INCREMENT для таблицы `users`
--
ALTER TABLE `users`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=18;

--
-- AUTO_INCREMENT для таблицы `user_roles`
--
ALTER TABLE `user_roles`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
