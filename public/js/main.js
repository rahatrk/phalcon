function removeAlert(){
    if(confirm("Вы уверены что хотите удалить?")){
        return true;
    }
    else{
        return false;
    }
}

function startHour(url, authUserHours, authUserStartEnd) {
    $.ajax({
        type: 'POST',
        url: url,
        data: {id: authUserHours, startEndId: authUserStartEnd, action: 'start'},
    });
}

function stopHour(url, authUserHours, authUserStartEnd) {
    $.ajax({
        type: 'POST',
        url: url,
        data: {id: authUserHours, startEndId: authUserStartEnd, action: 'stop'},
    });
}

function hideShowRows() {
    $('.hours_log tr').each(function () {
        if ($(this).hasClass('today')) {
            return true;
        } else {
            $(this).toggleClass('display_none');
        }
    });
}