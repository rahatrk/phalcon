Задачи по разработке системы трекинга времени.
Все части сайты должны быть написаны со всеми стандартами фремворка фалькон
Пример работы можно посмотреть по ссылке https://crm.growave.io/staff/
Требовании по проекту

Создать одно-модульный проект

Использовать ACL (ролевую систему гость, юзер и админ)

Понять работу DI для чего он нужен (чем отличается shared?)

Поиграться вьюшками
1) поменять лейаут
2) отключить лейаут
3) поменять темплейт акшена
4) использовать паршиалы, где они необходимы

При работе с базой обязательно использовать бинды

Использовать роутинг, урлы должны быть красивыми, также во всех вьюшках не использовать хардкод для вывода урлов (использовать сервис url)

Для формы логина добавить CSRF protection (Security service)

Пользовательсякая часть

1) страница авторизации - При каждой открытии страниц, система должна проверять человека на авторизованность и если он не авторизован перенаправлять его на страницу авторизации. У пользователя должна быть возможность поменять пароль и все.

2) После авторизации пользователь попадает на страницу часов, Там ему выводит большая кнопка Start/Stop через которую пользователь ука
ени показать когда он закончил работу.

3) Так же пользователь должен иметь возможность посмотреть свою историю прихода и ухода. разделять все времени по месяцам и выводить общее количество часов к конкретному месяцу который был выбран, если месяц не выбран показывать текущий месяц и статистику.

4) Система должна автоматически считать сколько дней в месяце и выводить сколько рабочих дней. так же сделать функцию ввода не рабочих дней.

Админская часть системы

1) Создание нового пользователя - в системе не должно быть система регистриации, пользователи должны создаватся только администратором в админ панели системы.
При создании администратор может указать
а) Имя пользвателя
б) логин
в) пароль
д) email

2) Удаления пользователя - системе должна быть функция удаления пользователя. При клике на него пользователь уже не может авторизоваться и выводится сообщение о том что такого пользователя не существует. Но в системе не удалять записи в базе данных а просто отметить его как не активного.

3) Просмотр всех пользователей - в системе должна быть страница где администратор может просмотреть всех пользователей. Страница должна вмещать всех пользователей и выводить когда они пришли в текущий день. Так же должна быть кнопка которая раскрывает всех пользователей и выводит информацию о пользователях на месяц. Если не выбран месяц который должен показываться то показать текущий. Администратор должен иметь возможность корректировать время когда пользователь пришел и ушёл. Добавить возможность добавлять время конкретному пользователю.

4) система ввода не рабочих дней - Открыть новую страницу где список не рабочих дней (Только праздники, суббота и воскресение не выводить). Администратор должен иметь возможность добавлять и удалять не рабочие дни. При создании не рабочего дня добавить галочку повторений. Если галочка чекнута то повторять не рабочий день в следующем году тоже.

5) Опоздании - Добавить страницу опозданий, где администратор указывает время когда начинается рабочий день, если пользователь нажимает на старт после указанного времени то считать его опоздавшим и записать в базу что он опоздал. Но так же Администратор должен иметь возможность удалить опоздание.