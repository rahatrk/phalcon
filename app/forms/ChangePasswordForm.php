<?php

use Phalcon\Forms\Form;
use Phalcon\Forms\Element\Text;
use Phalcon\Forms\Element\TextArea;
use Phalcon\Validation\Validator\PresenceOf;
use Phalcon\Validation\Validator\Email;
use Phalcon\Forms\Element\Password;
use Phalcon\Validation\Validator\StringLength;
use Phalcon\Validation\Validator\Confirmation;

class ChangePasswordForm extends Form
{
    public function initialize($entity = null, $options = null)
    {
        // Old password
//        $oldPassword = new Password('oldPassword');
//        $oldPassword->setLabel('Старый пароль');
//        $oldPassword->setFilters(['striptags', 'string']);
//        $oldPassword->setAttribute('required', 'true');
//        $oldPassword->addValidators([
//            new PresenceOf([
//                'message' => 'Old password is required'
//            ])
//        ]);
//        $this->add($oldPassword);

        // New Password
//        $password = new Password('password');
//        $password->setLabel('Password');
//        $password->setAttribute('required', 'true');
//        $password->addValidators([
//            new PresenceOf([
//                'message' => 'Password is required'
//            ])
//        ]);
//        $this->add($password);

        // Confirm Password
//        $repeatPassword = new Password('repeatPassword');
//        $repeatPassword->setLabel('Repeat Password');
//        $repeatPassword->setAttribute('required', 'true');
//        $repeatPassword->addValidators([
//            new PresenceOf([
//                'message' => 'Confirmation password is required'
//            ])
//        ]);
//        $this->add($repeatPassword);

        // Password
        $password = new Password('password', [
            'class' => 'form-control',
            'id'    => 'password'
        ]);

        $password->addValidators([
            new PresenceOf([
                'message' => 'Password is required'
            ]),
            new StringLength([
                'min' => 3,
                'messageMinimum' => 'Password is too short. Minimum 8 characters'
            ]),
            new Confirmation([
                'message' => 'Password doesn\'t match confirmation',
                'with' => 'confirmPassword'
            ])
        ]);

        $this->add($password);

        // Confirm Password
        $confirmPassword = new Password('confirmPassword', [
            'class' => 'form-control',
            'id'    => 'confirmPassword'
        ]);

        $confirmPassword->addValidators([
            new PresenceOf([
                'message' => 'The confirmation password is required'
            ])
        ]);

        $this->add($confirmPassword);
    }

    /**
     * Prints messages for a specific element
     */
    public function messages($name)
    {
        if ($this->hasMessagesFor($name)) {
            foreach ($this->getMessagesFor($name) as $message) {
                $this->flash->error($message);
            }
        }
    }
}
