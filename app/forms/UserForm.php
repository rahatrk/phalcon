<?php

use Phalcon\Forms\Form;
use Phalcon\Forms\Element\Text;
use Phalcon\Forms\Element\Password;
use Phalcon\Validation\Validator\PresenceOf;
use Phalcon\Validation\Validator\Email;
use Phalcon\Forms\Element\Check;
use Phalcon\Forms\Element\Select;

class UserForm extends Form
{
    public function initialize($entity = null, $options = null)
    {
        // UserName
        $username = new Text('username');
        $username->setLabel('Username');
        $username->setFilters(['alpha']);
        $username->setAttribute('required', 'true');
        $username->addValidators([
            new PresenceOf([
                'message' => 'Please enter your desired user name'
            ])
        ]);
        $this->add($username);

        // Last Name
        $lastname = new Text('lastname');
        $lastname->setLabel('Lastname');
        $lastname->setAttribute('required', 'true');
        $lastname->setFilters(['alpha']);
        $lastname->addValidators([
            new PresenceOf([
                'message' => 'Please enter your desired last name'
            ])
        ]);
        $this->add($lastname);

        // Email
        $email = new Text('email');
        $email->setLabel('E-Mail');
        $email->setAttribute('required', 'true');
        $email->setFilters('email');
        $email->addValidators([
            new PresenceOf([
                'message' => 'E-mail is required'
            ]),
            new Email([
                'message' => 'E-mail is not valid'
            ])
        ]);
        $this->add($email);

        // Password
        $password = new Password('password');
        $password->setLabel('Password');
        $password->setAttribute('required', 'true');
        $password->addValidators([
            new PresenceOf([
                'message' => 'Password is required'
            ])
        ]);
        $this->add($password);

        // Active user
        $active = new Check('active');
        $active->setLabel('Active');
        $this->add($active);

        // User role
        $userRolesList = UserRoles::getUserRolesList();
        $userRole = new Select('userRole', $userRolesList);
        $userRole->setLabel('User role');
        $userRole->setAttribute('required', 'true');
        $userRole->addValidators([
            new PresenceOf([
                'message' => 'User role is required'
            ])
        ]);
        $this->add($userRole);
    }
}
