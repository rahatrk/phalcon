<?php

/**
 * Route rules
 */

return[
    [
        'route' => '/home',
        'options' => ['controller' => 'index', 'action' => 'index'],
        'name' => 'home'
    ],
    [
        'route' => '/logout',
        'options' => ['controller' => 'session', 'action' => 'end'],
        'name' => 'logout'
    ],
    [
        'route' => '/login',
        'options' => ['controller' => 'session', 'action' => 'start'],
        'name' => 'login'
    ],
    [
        'route' => '/users',
        'options' => ['controller' => 'admin', 'action' => 'users'],
        'name' => 'users'
    ],
    [
        'route' => '/edit-user-hour/{userId}',
        'options' => ['controller' => 'admin', 'action' => 'editUserHour'],
        'name' => 'edit-user-hour'
    ],
    [
        'route' => '/admin/{id}/update-start-end/',
        'options' => ['controller' => 'admin', 'action' => 'updateStartEnd'],
        'name' => 'admin-update-start-end'
    ],
    [
        'route' => '/settings',
        'options' => ['controller' => 'settings', 'action' => 'index'],
        'name' => 'settings'
    ],
    [
        'route' => '/user-profile',
        'options' => ['controller' => 'index', 'action' => 'profile'],
        'name' => 'user-profile'
    ],
    [
        'route' => '/change-password',
        'options' => ['controller' => 'index', 'action' => 'changePassword'],
        'name' => 'change-password'
    ],
    [
        'route' => '/days-off',
        'options' => ['controller' => 'daysoff', 'action' => 'index'],
        'name' => 'days-off'
    ],
    [
        'route' => '/hours-update',
        'options' => ['controller' => 'index', 'action' => 'hoursUpdate'],
        'name' => 'hours-update'
    ],
];
