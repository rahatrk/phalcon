<?php

use \Phalcon\Mvc\Model;

/**
 * Class Permissions
 */
class Permissions extends Model
{
    /**
     * Initialize method for model.
     */
    public function initialize()
    {
        $this->setSource("permissions");

        $this->belongsTo('roleId', 'UserRoles', 'id', [
            'alias' => 'profile'
        ]);
    }
}
