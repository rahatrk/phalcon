<?php

use Phalcon\Mvc\Model;
use Phalcon\Mvc\Model\Relation;

class UserRoles extends Model {

    /**
     * Initialize method for model.
     */
    public function initialize()
    {
        $this->setSource('user_roles');

        $this->hasMany('id', 'Users', 'userRole', [
            'alias' => 'users',
            'foreignKey' => [
                'message' => 'Profile cannot be deleted because it\'s used on Users'
            ]
        ]);

        $this->hasMany('id', 'Permissions', 'roleId', [
            'alias' => 'permissions',
            'foreignKey' => [
                'action' => Relation::ACTION_CASCADE
            ]
        ]);
    }

    /**
     * Get user roles list
     * @return array
     */
    public static function getUserRolesList() {
        $roles = self::find();

        $rolesList = [];

        if(!empty($roles)) {
            foreach ($roles as $role) {
                $rolesList[$role->id] = $role->name;
            }
        }

        return $rolesList;
    }
}