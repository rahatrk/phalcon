<?php

class AclModel extends \Phalcon\Mvc\Model {

    public static function privateResourceList() {
        return [
            'index'      => ['index', 'profile', 'changePassword', 'hoursUpdate'],
            'admin'      => ['index', 'create', 'edit', 'delete', 'users', 'editUserHour', 'updateStartEnd'],
            'daysoff'    => ['index', 'create', 'edit', 'delete'],
            'settings'   => ['index'],
            'hours'      => ['index', 'update', 'updateTotal'],
        ];
    }

    /**
     * Checks if a controller is private or not
     *
     * @param string $controllerName
     * @return boolean
     */
    public static function isPrivate($controllerName)
    {
        $controllerName = strtolower($controllerName);
        $privateResources = self::privateResourceList();

        return isset($privateResources[$controllerName]);
    }
}