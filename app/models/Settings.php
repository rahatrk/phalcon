<?php

use Phalcon\Mvc\Model;

class Settings extends Model
{
    public function initialize()
    {
        $this->setSource("settings");
    }

    /**
     * Returns table name mapped in the model.
     *
     * @return string
     */
    public function getSource()
    {
        return 'settings';
    }

    /**
     * Get by key
     * @param $key
     * @return Model
     */
    public function getByKey($key)
    {
        $item = self::findFirst([
            'conditions' => 'key = :key:',
            'bind' => [
                'key' => $key
            ]
        ]);

        return $item;
    }

    /**
     * Get value by key
     * @param $key
     * @return Model\Resultset|\Phalcon\Mvc\Phalcon\Mvc\Model|null
     */
    public function getValueByKey($key)
    {
        $item = self::findFirst([
            'conditions' => 'key = :key:',
            'bind' => [
                'key' => $key
            ]
        ]);

        return $item ? $item->value : null;
    }
}
