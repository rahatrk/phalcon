<?php

use Phalcon\Mvc\Model;
use Phalcon\Validation;
use Phalcon\Validation\Validator\Email as EmailValidator;
use Phalcon\Validation\Validator\Uniqueness as UniquenessValidator;
use Phalcon\Validation\Validator\Email;
use Phalcon\Session\Adapter\Files as Session;

class Users extends Model
{
    /**
     * Initialize
     */
    public function initialize()
    {
        $this->setSource('users');
    }

    /**
     * Method validation
     */
    public function validation()
    {
        $validator = new Validation();
        
        $validator->add(
            'email',
            new EmailValidator([
            'message' => 'Invalid email given'
        ]));
        $validator->add(
            'email',
            new UniquenessValidator([
            'message' => 'Sorry, The email was registered by another user'
        ]));
        $validator->add(
            'username',
            new UniquenessValidator([
            'message' => 'Sorry, That username is already taken'
        ]));
        
        return $this->validate($validator);
    }

    /**
     * Find user by id
     * @param $userId
     * @return Model
     */
    public static function getUser($userId) {
        $user = Users::findFirst($userId);

        return $user;
    }

    /**
     * Return auth user id
     * @return false|mixed
     */
    public static function getAuthUserId() {
        $session = new Session();

        $auth = $session->get('auth');

        if ($auth) {
            return $auth['id'];
        }

        return false;
    }
}
