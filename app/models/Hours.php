<?php
use Phalcon\Mvc\Model;
use Phalcon\Mvc\Model\Resultset\Simple as Resultset;

class Hours extends Model
{
    /**
     *
     * @var integer
     */
    public $id;

    /**
     *
     * @var integer
     */
    public $usersId;

    /**
     *
     * @var string
     */
    public $total;

    /**
     *
     * @var string
     */
    public $less;

    /**
     *
     * @var string
     */
    public $late;

    /**
     *
     * @var integer
     */
    public $createdAt;

    /**
     * Initialize method for model.
     */
    public function initialize()
    {
        $this->setSource("hours");

        $this->belongsTo('usersId', 'Users', 'id', [
            'alias' => 'user'
        ]);

        $this->hasMany('id', 'StartEnd', 'hourId', [
            'alias' => 'startEnds',
            'foreignKey' => [
                'message' => 'User cannot be deleted because he/she has activity in the system'
            ]
        ]);
    }

    /**
     * Returns table name mapped in the model.
     *
     * @return string
     */
    public function getSource()
    {
        return 'hours';
    }

    /**
     * Allows to query a set of records that match the specified conditions
     *
     * @param mixed $parameters
     * @return Hours[]|Hours|\Phalcon\Mvc\Model\ResultSetInterface
     */
    public static function find($parameters = null)
    {
        return parent::find($parameters);
    }

    /**
     * Allows to query the first record that match the specified conditions
     *
     * @param mixed $parameters
     * @return Hours|\Phalcon\Mvc\Model\ResultInterface
     */
    public static function findFirst($parameters = null)
    {
        return parent::findFirst($parameters);
    }

    /**
     * Возвращает
     *
     * @param $createdAt
     * @param $userId
     * @return array|Hours|Hours[]|\Phalcon\Mvc\Model\ResultSetInterface
     */
    public function getByCreatedAt($createdAt, $userId)
    {
        $hours = self::find([
            'conditions' => 'createdAt LIKE :createdAt: AND usersId = :id:',
            'bind'       => [
                'createdAt' => $createdAt,
                'id'        => $userId
            ]
        ]);

        return $hours ? $hours->toArray() : $hours;
    }

    /**
     * Get late count by date
     * @param $createdAt
     * @return Hours|Hours[]|mixed|Model\ResultSetInterface
     */
    public function getLateCountByCreatedAt($createdAt)
    {
        $lateCountPerMonth = self::find([
            'conditions' => 'createdAt LIKE :createdAt: AND late = :late:',
            'bind'       => [
                'createdAt' => $createdAt,
                'late'      => 1
            ]
        ]);

        return $lateCountPerMonth ? $lateCountPerMonth->count() : $lateCountPerMonth;
    }

    /**
     * Get auth late count by date
     * @param $createdAt
     * @param $userId
     * @return Hours|Hours[]|mixed|Model\ResultSetInterface
     */
    public function getAuthLateCountByCreatedAt($createdAt, $userId)
    {
        $authUserLateCount = self::find([
            'conditions' => 'createdAt LIKE :createdAt: AND usersId = :id: AND late = :late:',
            'bind'       => [
                'createdAt' => $createdAt,
                'id'        => $userId,
                'late'      => 1
            ]
        ]);

        return $authUserLateCount ? $authUserLateCount->count() : $authUserLateCount;
    }

    /**
     * Find first by user and date
     * @param $userId
     * @param $createdAt
     * @return Hours|Model\ResultInterface
     */
    public static function findFirstByUserIdAndCreatedAt($userId, $createdAt)
    {
        $hour = self::findFirst([
            'usersId = ?0 AND createdAt = ?1',
            'bind' => [
                $userId,
                $createdAt
            ]
        ]);

        return $hour;
    }

    /**
     * Create todays counter
     *
     * @param $currentDate
     * @return Hours|\Phalcon\Mvc\Model\ResultInterface
     */
    public static function createTodaysCounter($currentDate)
    {
        $authUserId = Users::getAuthUserId();
        $hour = self::findFirstByUserIdAndCreatedAt($authUserId, $currentDate);

        if (!$hour) {
            $hour = new Hours();

            $hour->usersId = $authUserId;
            $hour->createdAt = date('Y-m-d');

            $hour->save();

            $startEnd = new StartEnd();
            $startEnd->hourId = $hour->id;

            $startEnd->save();
        }

        return $hour;
    }

    /**
     * Get by month and year
     * @param $monthId
     * @param $yearId
     * @return array|Model\ResultsetInterface
     */
    public static function getByMonthAndYear($monthId, $yearId)
    {
        $hours = self::query()
            ->columns('id')
            ->where('MONTH(createdAt) = :monthId: AND YEAR(createdAt) = :yearId:', [
                'monthId' => $monthId,
                'yearId'  => $yearId
            ])
            ->execute();

        return count($hours) ? $hours->toArray() : $hours;
    }

    /**
     * Get by month hour ids
     * @param $monthId
     * @param $yearId
     * @return array
     */
    public static function getByMonthHourIds($monthId, $yearId)
    {

        $hours = self::getByMonthAndYear($monthId, $yearId);

        if (!empty($hours)) {
            $idList = [];

            foreach ($hours as $hour) {
                $idList[] = $hour['id'];
            }
        }

        return $idList;
    }

    /**
     * Get years list
     * @return array
     */
    public static function getYearsList() {
        $item = new Hours();
        $sql = 'SELECT DISTINCT EXTRACT(YEAR FROM createdAt) AS year FROM hours';
        $data = new Resultset(null, $item, $item->getReadConnection()->query($sql));
        $hours = $data->toArray();

        $yearsList = [];

        if (!empty($hours)) {
            foreach ($hours as $item) {
                $yearsList[] = [
                    'name' => $item['year'],
                    'value' => $item['year'],
                ];
            }
        }

        return $yearsList;
    }

    public static function findByUserIdAndCreatedAt($userId, $createdAt)
    {
        $hours = self::query()
            ->columns(['id', 'usersId', 'createdAt', 'total'])
            ->where('usersId = :userId: AND createdAt = :createdAt:', [
                'userId' => $userId,
                'createdAt'  => $createdAt
            ])
            ->execute();

        $data = $hours->toArray();

        if (!empty($data[0])) {

            return $data[0];
        }

        return [];
    }
}
