<?php

use Phalcon\Mvc\Model;

class Daysoff extends Model
{
    public function initialize()
    {
        $this->setSource('days_off');
    }

    /**
     * Returns all non-working days
     */
    public function getAllByMonth($month)
    {
        $items = self::find([
            'conditions' => 'month = :month: AND (repeat = :repeatNo: AND date = :date:) OR repeat = :repeatYes: AND month = :month:',
            'bind' => [
                'month' => $month,
                'repeatNo' => 'N',
                'date' => date('Y'),
                'repeatYes' => 'Y',
            ]
        ]);

        return $items;
    }
}
