<?php

use Phalcon\Mvc\Model;

class StartEnd extends Model
{
    /**
     *
     * @var integer
     */
    public $id;

    /**
     *
     * @var integer
     */
    public $hourId;

    /**
     *
     * @var string
     */
    public $start;

    /**
     *
     * @var string
     */
    public $stop;

    /**
     * Initialize method for model.
     */
    public function initialize()
    {
        $this->setSource("start_end");

        $this->belongsTo('hourId', 'Hours', 'id', [
            'alias' => 'hour'
        ]);
    }

    /**
     * Returns table name mapped in the model.
     *
     * @return string
     */
    public function getSource()
    {
        return 'start_end';
    }

    /**
     * Allows to query a set of records that match the specified conditions
     *
     * @param mixed $parameters
     * @return StartEnd[]|StartEnd|\Phalcon\Mvc\Model\ResultSetInterface
     */
    public static function find($parameters = null)
    {
        return parent::find($parameters);
    }

    /**
     * Allows to query the first record that match the specified conditions
     *
     * @param mixed $parameters
     * @return StartEnd|\Phalcon\Mvc\Model\ResultInterface
     */
    public static function findFirst($parameters = null)
    {
        return parent::findFirst($parameters);
    }

    /**
     * Find by hour id
     * @param $hourId
     * @return Model\ResultSetInterface|StartEnd|StartEnd[]
     */
    public static function findByHourId($hourId)
    {
        $startEnds = self::find([
            'conditions' => 'hourId = :hourId:',
            'bind' => [
                'hourId' => $hourId
            ]
        ]);

        return $startEnds;
    }

    /**
     * Find by first by hour id
     * @param $hourId
     * @return Model\ResultInterface|StartEnd
     */
    public function findFirstByHourId($hourId)
    {
        $firstStartEnd = self::findFirst([
            'conditions' => 'hourId = :hourId:',
            'bind' => [
                'hourId' => $hourId,
            ],
        ]);

        return $firstStartEnd;
    }

    /**
     * Find hous ids
     * @param $ids
     * @return array|Model\ResultsetInterface
     */
    public static function getByUserIdAndHourIds($userId, $ids)
    {
        $startEnds = self::query()
            ->columns([
                'StartEnd.id',
                'StartEnd.hourId',
                'StartEnd.start',
                'StartEnd.stop',
                'Hours.createdAt',
                'Hours.total'
            ])
            ->join('Hours', 'Hours.id = StartEnd.hourId')
            ->where("usersId = :userId:", [
                'userId' => $userId
            ])
            ->inWhere("StartEnd.hourId", $ids)
            ->execute();

        return count($startEnds) ? $startEnds->toArray() : $startEnds;
    }
}
