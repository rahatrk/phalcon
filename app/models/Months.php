<?php

use Phalcon\Mvc\Model;
use Phalcon\Validation;

class Months extends Model
{
    /**
     * Initialize
     */
    public function initialize()
    {
        $this->setSource('months');
    }

    /**
     * Get months list
     * @return array
     */
    public static function getMonthsList() {

        $months = Months::find();
        $monthsList = [];

        foreach ($months as $item) {
            $monthsList[] = [
                'name' => $item->name,
                'value' => $item->value,
            ];
        }

        return $monthsList;
    }
}
