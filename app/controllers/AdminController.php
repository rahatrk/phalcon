<?php

use Phalcon\Paginator\Adapter\Model as PaginatorModel;
use Phalcon\Http\Response;
use Phalcon\Validation;
use Phalcon\Validation\Validator\PresenceOf;
use Phalcon\Validation\Validator\Callback;
use Phalcon\Validation\Validator\Regex;

/**
 * Class AdminController
 */
class AdminController extends ControllerBase
{
    /**
     * @var int $hourForDay
     */
    public $hourForDay = 9;

    /**
     * @var $day
     */
    protected $day;

    /**
     * @var $month
     */
    protected $month;

    /**
     * @var $year
     */
    protected $year;

    /**
     * @var $dateTime
     */
    protected $dateTime;

    /**
     * @var $validation
     */
    protected $validation;

    /**
     * Method initialize
     */
    public function initialize()
    {
        $this->day = date('d');
        $this->month = date('m');
        $this->year = date('Y');
        $this->dateTime = new CustomDateTime();
        $this->validation = new AdminValidation();

        $this->tag->setTitle('Admin');
    }

    /**
     * Action index
     */
    public function indexAction()
    {
        $currentPage = (int)$_GET['page'];
        $users = Users::find();

        $paginator = new PaginatorModel(
            [
                'data' => $users,
                'limit' => 5,
                'page' => $currentPage,
            ]
        );

        $this->view->page = $paginator->getPaginate();
    }

    /**
     * Action users
     */
    public function usersAction()
    {
        $currentPage = (int)$_GET['page'];
        $users = Users::find();

        $paginator = new PaginatorModel(
            [
                'data' => $users,
                'limit' => 5,
                'page' => $currentPage,
            ]
        );

        $this->view->page = $paginator->getPaginate();
    }

    /**
     * Action create user
     */
    public function createAction()
    {
        $form = new UserForm();

        if ($this->request->isPost()) {

            $lastname = $this->request->getPost('lastname');
            $username = $this->request->getPost('username', 'alphanum');
            $email = $this->request->getPost('email', 'email');
            $password = $this->request->getPost('password');
            $userRole = $this->request->getPost('userRole');

            $user = new Users();
            $user->lastname = $lastname;
            $user->username = $username;
            $user->password = sha1($password);
            $user->email = $email;
            $user->active = 'Y';
            $user->created_at = new Phalcon\Db\RawValue('now()');
            $user->userRole = $userRole;

            if ($user->save() == false) {
                foreach ($user->getMessages() as $message) {
                    $this->flash->error((string)$message);
                }
            } else {
                $this->response->redirect('users');

                return;
            }
        }

        $this->view->form = $form;
    }

    /**
     * Action edit user
     */
    public function editAction()
    {
        $userId = (int)$_GET['id'];
        $user = Users::findFirst($userId);

        $this->view->user = $user;
        if (!empty($user)) {
            $form = new UserForm();

            if ($this->request->isPost()) {

                $lastname = $this->request->getPost('lastname');
                $username = $this->request->getPost('username', 'alphanum');
                $email = $this->request->getPost('email', 'email');
                $password = $this->request->getPost('password');
                $active = $this->request->getPost('active');
                $userRole = $this->request->getPost('userRole');

                $user->lastname = $lastname;
                $user->username = $username;
                $user->password = sha1($password);
                $user->email = $email;
                $user->active = !empty($active) ? 'Y' : 'N';
                $user->created_at = new Phalcon\Db\RawValue('now()');
                $user->userRole = $userRole;

                if ($user->save() == false) {
                    foreach ($user->getMessages() as $message) {
                        $this->flash->error((string)$message);
                    }
                } else {
                    $this->response->redirect('users');

                    return;
                }
            }

            $this->view->form = $form;

        } else {
            $this->response->redirect('errors/show404');
        }
    }

    /**
     * Action delete user
     */
    public function deleteAction()
    {
        $userId = (int)$_GET['id'];
        $user = Users::findFirst($userId);

        if (!empty($user)) {
            $user->active = 'N';
            $user->save();
        }

        $this->response->redirect('admin/index');

        return;
    }

    /**
     * Action edit user hour
     * @param $userId
     */
    public function editUserHourAction($userId)
    {
        $user = Users::findFirst($userId);

        if (empty($user)) {
            $this->response->redirect('errors/show404');
        }

        if ($this->request->get('day')) {
            $this->day = $this->request->get('day');
        }

        if ($this->request->get('month')) {
            $this->month = $this->request->get('month');
        }

        if ($this->request->get('year')) {
            $this->year = $this->request->get('year');
        }

        $createdAt = $this->dateTime->convertToDate($this->year, $this->month, $this->day);
        $hour = Hours::findByUserIdAndCreatedAt($userId, $createdAt);

        if (!empty($hour)) {
            $startEnds = StartEnd::findByHourId($hour['id'])->toArray();
        }

        $this->view->user = $user;

        $daysoff = new Daysoff();
        $notWorkingDays = $daysoff->getAllByMonth($this->month);

        $this->view->hour = $hour;
        $this->view->startEnds = $startEnds;
        $this->view->monthsList = Months::getMonthsList();
        $this->view->yearList = Hours::getYearsList();
        $this->view->daysMonthList = $this->dateTime->getDates($this->month, $this->year, $notWorkingDays);
        $this->view->currentDay = $this->day;
        $this->view->currentMonth = $this->month;
        $this->view->currentYear = $this->year;
    }

    /**
     * Update user start end hour
     * @param $id
     * @return Response|\Phalcon\Http\ResponseInterface
     */
    public function updateStartEndAction($id)
    {
        if ($this->request->isPost()) {
            $entity   = [];
            $messages = [];

            if ($month = $this->request->get('month')) {
                $this->month = $month;
            }

            $notWorkingDays = $this->getDaysoffModel()->getAllByMonth($this->month);

            $validationMessages = $this->validation->validate($this->request->getPost());
            $response = new Response();

            $validationArrayMessages = [];

            foreach ($validationMessages as $validationMessage) {
                $validationArrayMessages[] = $validationMessage->getMessage();
            }

            if(!count($validationArrayMessages)) {

                $assignment = [];

                $startEnd  = StartEnd::findFirst($id);

                $startEnd->assign([
                    'start' => $this->request->getPost('start'),
                    'stop'  => $this->request->getPost('stop')
                ]);

                if(!$startEnd->save()) {
                    $messages = $startEnd->getMessages();
                } else {

                    $hour      = Hours::findFirst($startEnd->hour->id);
                    $startEnds = $this->getStartEndModel()->findByHourId($hour->id);
                    $firstStartEnd = $this->getStartEndModel()->findFirstByHourId($hour->id);
                    $total     = $this->dateTime->getTotalDifference($startEnds);
                    $assignment['total'] = $total;

                    if($firstStartEnd->id == $id) {

                        $beginning = $this->getSettingsModel()->getValueByKey('beginning') ?: $this->beginning;

                        if (!$this->dateTime->isNotWorkingDay($hour->createdAt, $notWorkingDays)) {
                            if (strtotime($beginning) < strtotime($this->request->getPost('start'))) {
                                $assignment['late'] = 1;
                            } else {
                                $assignment['late'] = 0;
                            }
                        }
                    }

                    if (!$this->dateTime->isNotWorkingDay($hour->createdAt, $notWorkingDays)) {

                        $assignment['less'] = (($this->hourForDay * 3600) > $this->dateTime->parseHour($total)) ?
                            $this->dateTime->getDiffBySecond($this->hourForDay * 3600, $this->dateTime->parseHour($total)) : null;
                    }

                    $hour->assign($assignment);

                    if(!$hour->save()) {
                        $messages = $hour->getMessages();
                    } else {
                        $entity['hourId'] = $startEnd->hour->id;
                        $entity['assignment'] = $assignment;
                        $entity['action'] = 'update';
                    }
                }
            }

            if (count($messages)) {
                $response->setStatusCode(500, 'Internal Server Error');
                $response->setContent(json_encode($messages));
                return $this->response->redirect($this->request->getHTTPReferer());
            } else {
                $entity['validation'] = $validationArrayMessages;

                $response->setStatusCode(200, 'OK');
                $response->setContent(json_encode($entity));
            }

            return $this->response->redirect($this->request->getHTTPReferer());
        }
    }

    /**
     * Return Daysoff model
     * @return Daysoff
     */
    protected function getDaysoffModel()
    {
        return new Daysoff();
    }

    /**
     * Return StartEnd model
     * @return StartEnd
     */
    protected function getStartEndModel()
    {
        return new StartEnd();
    }

    /**
     * Return Hours model
     * @return Hours
     */
    protected function getHoursModel()
    {
        return new Hours();
    }

    /**
     * Return Settings model
     * @return Settings
     */
    protected function getSettingsModel()
    {
        return new Settings();
    }
}
