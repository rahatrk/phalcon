<?php

use Phalcon\Mvc\Controller;
use Phalcon\Mvc\Dispatcher;
use Phalcon\Acl\Adapter\Memory as AclList;

/**
 * Class ControllerBase
 */
class ControllerBase extends Controller
{
    /**
     * @var $identity
     */
    protected $identity;

    /**
     * Method initialize
     */
    protected function initialize()
    {
        $this->identity = $this->session->get('auth')['id'];
        $this->view->setTemplateAfter('main');
    }

    /**
     * Execute before the router so we can determine if this is a private controller, and must be authenticated, or a
     * public controller that is open to all.
     *
     * @param Dispatcher $dispatcher
     * @return boolean
     */
    public function beforeExecuteRoute(Dispatcher $dispatcher)
    {
        $controllerName = $dispatcher->getControllerName();
        $acl = new AclList();

        // Only check permissions on private controllers
        if (AclModel::isPrivate($controllerName)) {

            // Get the current identity
            $identity = $this->session->get('auth');

            // If there is no identity available the user is redirected to index/index
            if (!is_array($identity)) {

                $this->flash->notice('You don\'t have access to this module: private. To get to this page you must log in');

                $dispatcher->forward([
                    'controller' => 'session',
                    'action' => 'start'
                ]);
                return false;
            }

            $actionName = $dispatcher->getActionName();

            // Check if the user have permission to the current option
            if (!$acl->isAllowed($identity['userRole'], $controllerName, $actionName)) {
                $this->flash->notice('You don\'t have access to this module: ' . $controllerName . ':' . $actionName);

                if ($acl->isAllowed($identity['userRole'], $controllerName, 'index')) {
                    $dispatcher->forward([
                        'controller' => $controllerName,
                        'action' => 'index'
                    ]);
                } else {
                    $dispatcher->forward([
                        'controller' => 'user_control',
                        'action' => 'index'
                    ]);
                }

                return false;
            }
        }
    }
}
