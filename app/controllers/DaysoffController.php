<?php
use Phalcon\Paginator\Adapter\Model as PaginatorModel;
use Phalcon\Forms\Element\Password;

/**
 * Class DaysoffController
 */
class DaysoffController extends ControllerBase
{
    /**
     * Method initialeze
     */
    public function initialize()
    {
        $this->tag->setTitle('Admin');
        $this->view->setTemplateAfter('admin');
    }

    /**
     * Action days off list
     */
    public function indexAction()
    {
        $days = Daysoff::find();

        $this->view->days = $days;
    }

    /**
     * Action create days off
     */
    public function createAction()
    {
        if ($this->request->isPost()) {
            $daysoff = new Daysoff();
            $daysoff->month = $this->request->getPost('month');;
            $daysoff->day = $this->request->getPost('day');
            $daysoff->repeat =  $this->request->getPost('repeat') ?: 'N';
            $daysoff->date = date('Y');

            if(!$daysoff->save()) {
                $this->session->set('error_message', $daysoff->getMessage());
            } else {

                $_POST = [];
                $this->session->set('success_message', ['success' => 'Not working day created successfully']);

                return $this->response->redirect('daysoff/index');
            }
        }
        $this->view->monthsList = Months::getMonthsList();
    }

    /**
     * Action delete days off
     */
    public function deleteAction()
    {
        $id = (int)$_GET['id'];
        $daysoff = Daysoff::findFirst($id);

        if (!empty($daysoff)) {
            $daysoff->delete();
        }

        $this->response->redirect('daysoff/index');

        return;
    }
}
