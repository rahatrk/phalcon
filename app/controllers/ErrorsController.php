<?php

/**
 * ErrorsController
 *
 * Manage errors
 */
class ErrorsController extends ControllerBase
{
    /**
     * Initialize
     */
    public function initialize()
    {
        $this->tag->setTitle('Oops!');
        parent::initialize();
    }

    /**
     * Action show 404
     */
    public function show404Action()
    {

    }

    /**
     * Action show 401
     */
    public function show401Action()
    {

    }

    /**
     * Action show 500
     */
    public function show500Action()
    {

    }
}
