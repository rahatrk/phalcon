<?php

/**
 * Class SettingsController
 */
class SettingsController extends ControllerBase
{
    /**
     * Method initialize
     */
    public function initialize()
    {
        $this->tag->setTitle('Admin');
        $this->view->setTemplateAfter('admin');
    }

    /**
     * Action index
     * @return mixed
     */
    public function indexAction()
    {
        $this->view->beginning = $this->getModel()->getValueByKey('beginning');
        $this->view->maxLate = $this->getModel()->getValueByKey('max_late');

        if($this->request->isPost()) {

            $saveUpdateMessage = null;
            $settings = $this->request->getPost('settings');

            foreach ($settings as $key => $value) {
                $setting = $this->getModel()->getByKey($key);

                if($setting) {
                    $setting->assign([
                        'key' => $key,
                        'value' => $value
                    ]);

                    if(!$setting->save()) {
                        $saveUpdateMessage = $setting->getMessages();
                    }
                } else {
                    $setting = $this->getModel();

                    $setting->key = $key;
                    $setting->value = $value;

                    if(!$setting->save()) {
                        $saveUpdateMessage = $setting->getMessages();
                    }
                }
            }

            if ($saveUpdateMessage) {
                $this->session->set('error_message', $saveUpdateMessage);
            } else {
                $this->session->set('success_message', ['success' => 'Successfully saved']);
            }

            return $this->response->redirect('settings');
        }
    }

    /**
     * Return Setting model
     * @return Settings
     */
    protected function getModel()
    {
        return new Settings();
    }
}

