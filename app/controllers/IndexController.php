<?php
use Phalcon\Acl;
use Phalcon\Acl\Adapter\Memory as AclList;
use Phalcon\Http\Response;

/**
 * Class IndexController
 */
class IndexController extends ControllerBase
{
    /**
     * @var $identity
     */
    protected $identity;

    /**
     * @var int $hourForDay
     */
    public $hourForDay = 9;

    /**
     * @var string $beginning
     */
    public $beginning = '09:00:00';

    /**
     * @var null $total
     */
    public $total = null;

    /**
     * @var null $less
     */
    public $less = null;

    /**
     * @var null $updateUrl
     */
    public $updateUrl = null;

    /**
     * @var $month
     */
    protected $month;

    /**
     * @var $year
     */
    protected $year;

    /**
     * @var $settings
     */
    protected $settings;

    /**
     * @var $dateTime
     */
    protected $dateTime;

    /**
     * Methos initialize
     */
    public function initialize()
    {
        $auth = $this->session->get('auth');

        $this->identity = $auth['id'];

        $this->tag->setTitle('Welcome');
        $this->settings = new Settings();
        $this->month = date('m');
        $this->year = date('Y');
        $this->dateTime = new CustomDateTime();

        parent::initialize();
    }

    /**
     * Action index
     */
    public function indexAction()
    {
        $currentDate = date('Y-m-d');

        if ($month = $this->request->get('month')) {
            $this->month = $month;
        }

        if ($year = $this->request->get('year')) {
            $this->year = $year;
        }

        $monthHourList = Hours::getByMonthHourIds($this->month,$this->year);
        $startEndList = $this->getStartEndModel()->getByUserIdAndHourIds($this->identity, $monthHourList);

        // create user current day counter
        $hour = Hours::createTodaysCounter($currentDate);

        $authUserHours = Hours::findFirstByUserIdAndCreatedAt($this->identity, $currentDate);
        $authUserStartEnd = StartEnd::findByHourId($authUserHours->id);

        foreach ($authUserStartEnd as $item) {
            $authUserStartEndId = $item->id;
        }

        $this->view->authUserHours = $authUserHours;
        $this->view->authUserStartEndId = $authUserStartEndId;
        $this->view->url = $this->url->get('hours-update');
        $this->view->monthsList = Months::getMonthsList();
        $this->view->yearList = Hours::getYearsList();
        $this->view->currentMonth = $this->month;
        $this->view->currentYear = $this->year;
        $this->view->user = Users::getUser($this->identity);

        $totalSecondPerMonth = $this->getTotalSecondPerMonth($this->month, $this->year);
        $notWorkingDays = $this->getDaysoffModel()->getAllByMonth($this->month);
        $datesMonth = $this->dateTime->getDates($this->month, $this->year, $notWorkingDays);
        $workingDaysCount = $this->getWorkingDaysCount($datesMonth);
        $this->view->startEnds = array_pop($this->getStartEndModel()->findByHourId($hour->id)->toArray());
        $this->view->startEndList = $startEndList;

        $this->view->totalPerMonth = $this->getTotalPerMonth($totalSecondPerMonth);
        $this->view->workingHoursCount = $workingDaysCount * ($this->hourForDay - 1);
        $this->view->percentOfTotal = $this->getPercentOfTotal($workingDaysCount, $totalSecondPerMonth);
        $this->view->authUserlateCount = $this->getAuthUserLateCount($this->month, $this->year);
        $this->view->lateUsers = $this->getBeenLateUsers($this->month, $this->year);
        $this->view->currentDate = $currentDate;
        $this->view->datesMonth = $datesMonth;

        $this->tag->setTitle('Staff Growave');
    }

    /**
     * Action user profile
     */
    public function profileAction()
    {
        $user = Users::findFirst($this->identity);
        $this->view->user = $user;

        if (!empty($user)) {
            $form = new UserForm();

            if ($this->request->isPost()) {

                $lastname = $this->request->getPost('lastname');
                $user->lastname = $lastname;

                if ($user->save() == false) {
                    foreach ($user->getMessages() as $message) {
                        $this->flash->error((string) $message);
                    }
                } else {
                    $this->response->redirect('index/profile');

                    return;
                }
            }

            $this->view->form = $form;
        }
        else {
            $this->response->redirect('errors/show404');
        }
    }

    /**
     * Action change user password
     */
    public function changePasswordAction()
    {
        $form = new ChangePasswordForm();

        if ($this->request->isPost()) {

            if ($form->isValid($this->request->getPost())) {
                $user = Users::findFirst($this->identity);

                $user->assign([
                    'password' => sha1($this->request->getPost('password'))
                ]);

                if (!$user->save()) {
                    $this->flash->error($user->getMessages());
                } else {
                    $this->flash->success('Your password was successfully changed');

                    $_POST = [];

                    return $this->response->redirect('index/profile');
                }
            }
        }

        $this->view->authUser = $this->identity;
        $this->view->form = $form;
    }

    /**
     * Action hours update
     */
    public function hoursUpdateAction()
    {
//        if ($this->request->isPost()) {

            $id = $this->request->get('id');
            $startEndId = $this->request->get('startEndId');

            $hour = Hours::findFirst($id);

            $firstStartEnd = $this->getStartEndModel()->findFirstByHourId($hour->id);
            $notWorkingDays = $this->getDaysoffModel()->getAllByMonth($this->month);

            $message = [];
            $entity = [];

            if ($this->request->get('action') == 'start') {

                if (!$firstStartEnd->start) {
                    $beginning = $this->settings->getValueByKey('beginning') ?: $this->beginning;

                    if (!$this->dateTime->isNotWorkingDay(date('H:i:s'), $notWorkingDays)) {
                        if (strtotime($beginning) < strtotime('now')) {
                            $entity['late'] = 1;
                        }
                    }
                }

                $startEnd = StartEnd::findFirst($startEndId);

                $startEnd->assign([
                    'start' => date('H:i:s')
                ]);

                if (!$startEnd->save()) {
                    $message = $startEnd->getMessages();
                } else {

                    $entity['less'] = null;
                }
            }

            if ($this->request->get('action') == 'stop') {
                $startEnd = StartEnd::findFirst($startEndId);

                $startEnd->assign([
                    'stop' => date('H:i:s')
                ]);

                if (!$startEnd->save()) {
                    $message = $startEnd->getMessages();
                }

                $newStartEnd = $this->getStartEndModel();
                $newStartEnd->hourId = $id;

                if (!$newStartEnd->save()) {
                    $message = $newStartEnd->getMessages();
                }

                $this->updateUrl = $this->url->get(['for' => 'hours-update', 'id' => $id, 'startEndId' => $newStartEnd->id]);

                $startEnds = $this->getStartEndModel()->findByHourId($hour->id);
                $this->total = $this->dateTime->getTotalDifference($startEnds);

                if (!$this->dateTime->isNotWorkingDay(date('H:i:s'), $notWorkingDays)) {
                    $this->less = (($this->hourForDay * 3600) > $this->dateTime->parseHour($this->total)) ?
                        $this->dateTime->getDiffBySecond($this->hourForDay * 3600, $this->dateTime->parseHour($this->total)) : null;
                }

                $entity['total'] = $this->total;
                $entity['less'] = $this->less;
            }

            $hour->assign($entity);

            if (!$hour->save()) {
                $message = $hour->getMessages();
            }

            $response = new Response();

            if (count($message)) {
                $response->setStatusCode(500, 'Internal Server Error');
                $response->setContent(json_encode($message));
            } else {
                return $this->response->redirect('home');

                $response->setStatusCode(200, 'OK');

                $response->setContent(json_encode([
                    'success'   => true,
                    'updateUrl' => $this->updateUrl,
                    'action'    => $this->request->getPost('action'),
                    'startEnds' => $hour->startEnds,
                    'hourId'    => $hour->id,
                    'total'     => $this->total,
                    'less'      => $this->less,
                    'late'      => $entity['late'] ?: 0,
                ]));
            }

        return $this->response->redirect('home');

        return $response;
//        }
    }

    /**
     * Action update total
     */
    public function updateTotalAction($id)
    {
        $hour = Hours::findFirst($id);
        $startEnds = $this->getStartEndModel()->findByHourId($hour->id);

        $hour->assign([
            'total' => $this->dateTime->getTotalDifference($startEnds)
        ]);

        $response = new Response();

        if (!$hour->save()) {
            $response->setStatusCode(500, 'Internal Server Error');
            $response->setContent(json_encode($hour->getMessages()));
        } else {

            $response->setStatusCode(200, 'OK');
            $response->setContent(json_encode([
                'hourId' => $hour->id,
                'total'  => $this->dateTime->getTotalDifference($startEnds)
            ]));
        }

        return $response;
    }

    /**
     * Returns the count of business days
     */
    protected function getWorkingDaysCount($datesMonth)
    {
        $workingDaysCount = 0;

        foreach ($datesMonth as $dateMonth) {
            if ($dateMonth['working_day']) {
                $workingDaysCount++;
            }
        }

        return $workingDaysCount;
    }

    /**
     * Returns the timestamp of the sum of hours worked
     */
    protected function getTotalSecondPerMonth($month, $year)
    {
        $createdAt = $year . '-' . $month . '%';
        $hours = $this->getModel()->getByCreatedAt($createdAt, $this->identity);

        return $this->dateTime->getTotalSecondOfHours($hours);
    }

    /**
     * Returns the amount of hours worked in month
     */
    protected function getTotalPerMonth($totalSecondPerMonth)
    {
        $hour = 0;
        $minute = 0;

        if ($totalSecondPerMonth >= 60) {
            $hour = floor($totalSecondPerMonth / 60 / 60);
            $minute = ($totalSecondPerMonth / 60) % 60;
        }

        if (strlen((string)$hour) == 1) {
            $hour = '0' . $hour;
        }

        if (strlen((string)$minute) == 1) {
            $minute = '0' . $minute;
        }

        return $hour . ':' . $minute;
    }

    /**
     * Returns the percentage of hours worked
     */
    protected function getPercentOfTotal($workingDaysCount, $totalSecondPerMonth)
    {
        $percentOfTotal = 0;

        if ($totalSecondPerMonth >= 60) {

            $workingSecondsCount = $workingDaysCount * ($this->hourForDay - 1) * 60 * 60;
            $percentOfTotal = round($totalSecondPerMonth / ($workingSecondsCount / 100), 2);
        }

        return $percentOfTotal;
    }

    /**
     * Find count late auth user
     */
    protected function getAuthUserLateCount($month, $year)
    {
        $createdAt = $year . '-' . $month . '%';

        return $this->getModel()->getAuthLateCountByCreatedAt($createdAt, $this->identity);
    }

    /**
     * Find late users
     */
    protected function getBeenLateUsers($month, $year)
    {
        $createdAt = $year . '-' . $month . '%';

        $lateUsers = $this->modelsManager->createBuilder()
            ->from('Users')
            ->columns([
                'Users.id',
                'Users.lastname',
                'SUM(uh.late) AS beenLate',
            ])
            ->join('Hours', 'uh.usersId = Users.id', 'uh')
            ->where('uh.late = 1')->where('createdAt LIKE "' . $createdAt . '"')
            ->groupBy('Users.id')->orderBy('SUM(late) DESC')->limit(3)->getQuery()
            ->execute();

        return $lateUsers;
    }

    /**
     * Return Hours model
     */
    protected function getModel()
    {
        return new Hours();
    }

    /**
     * Return StartEnd model
     */
    protected function getStartEndModel()
    {
        return new StartEnd();
    }

    /**
     * Return Daysoff model
     */
    protected function getDaysoffModel()
    {
        return new Daysoff();
    }
}
