{{ content() }}

<div class="row">
    <div class="col-8 offset-2">
        <form method="POST" action="" autocomplete="off">
            <div class="center scaffold">
                <h2 class="text-center">Изменить пароль:</h2>

                <div class="form-group">
                    <label for="password">Password</label>
                    {{ form.render("password") }}
                    {{ form.messages('password') }}
                </div>

                <div class="form-group">
                    <label for="confirmPassword">Confirm Password</label>
                    {{ form.render("confirmPassword") }}
                    {{ form.messages('confirmPassword') }}
                </div>

                <div class="form-group">
                    {{ submit_button("Изменить", "class": "btn btn-primary") }}
                </div>
            </div>
        </form>
    </div>
</div>